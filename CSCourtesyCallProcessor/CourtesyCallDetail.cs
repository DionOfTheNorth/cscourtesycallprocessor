namespace CSCourtesyCallProcessor
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("customerservice.CourtesyCallDetails")]
    public partial class CourtesyCallDetail
    {
        public int Id { get; set; }

        public DateTime? LoadDate { get; set; }

        public int TitanBookingReference { get; set; }

        public int TitanCompanyId { get; set; }

        [Required]
        [StringLength(160)]
        public string TitanCompanyName { get; set; }

        public int ProductId { get; set; }

        public int OfficeId { get; set; }

        [Required]
        [StringLength(150)]
        public string OfficeName { get; set; }

        [Required]
        [StringLength(50)]
        public string ContactFirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string ContactLastName { get; set; }

        [Required]
        [StringLength(25)]
        public string ContactPhoneNumber { get; set; }

        [StringLength(50)]
        public string ContactEmail { get; set; }

        public int CentreNumber { get; set; }

        public DateTime? NotificationDueDate { get; set; }

        public DateTime? TerminationDate { get; set; }

        public int AmendmentParentId { get; set; }

        public int TicketReference { get; set; }

        public int StatusId { get; set; }
    }
}
