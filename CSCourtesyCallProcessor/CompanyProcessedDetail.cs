namespace CSCourtesyCallProcessor
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CompanyProcessedDetail : DbContext
    {
        public CompanyProcessedDetail()
            : base("name=CompanyProcessedDetail")
        {
        }

        public virtual DbSet<NotificationDDCompanySummary> NotificationDDCompanySummaries { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
