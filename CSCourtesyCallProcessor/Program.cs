﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using CSCourtesyCallProcessor.CSTS_ExternalIntegrations;
using Dapper;
using Logging;

namespace CSCourtesyCallProcessor
{
    internal static class Program
    {
        private static readonly Logger Logger = new Logger();

        private static void Main(string[] args)
        {
            Logger.Log("CCP - Starting Run", 0);
            Logger.Log("CCP - Getting Calls", 0);
            var courtesyCallDetailsList = GetCourtesyCallDetailList();
            foreach (var courtesyCallDetail in courtesyCallDetailsList)
            {
                CreateTicket(courtesyCallDetail);
            }
        }

        private static DbConnection CreateConnection()
        {
            var connection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["CourtesyCallDetails"].ConnectionString);

            //new SqlConnection(@"Data Source=reg10dbs89bpm.regus.local;" +
            //                             @"Initial Catalog=SORCS;User Id=bpm_sa;Password=bpm_sa");
            return connection;
        }

        private static List<CourtesyCallDetail> GetCourtesyCallDetailList()
        {
            using (var connection = CreateConnection())
            {
                return
                    connection.Query<CourtesyCallDetail>("[customerservice].[getCourtesyCallDetails]",
                        new {numberOfCallsToRetreive = 10}, commandType: CommandType.StoredProcedure).ToList();
            }
        }

        private static void CreateTicket(CourtesyCallDetail courtesyCallDetail)
        {
            Logger.Log("Passing to TW", 0);
            var courtesyCallCreate =
                new CSTS_ExternalIntegrationsPortTypeClient("CSTS_ExternalIntegrationsSoap");

            var description = String.Format("Please review the following courtesy call:\n\n" +
                                            "Notification Due Date:\t{0}\n" +
                                            "Termination Date:\t{1}\n" +
                                            "Titan Booking Reference:\t{2}\n" +
                                            "Titan Company:\t{3}({4})" +
                                            "Office Name:\t{5}\n\n" +
                                            "First Name:\t{6}\n" +
                                            "Last Name:\t{7}\n" +
                                            "Phone Number:\t{8}",
                courtesyCallDetail.NotificationDueDate,
                courtesyCallDetail.TerminationDate,
                courtesyCallDetail.TitanBookingReference, courtesyCallDetail.TitanCompanyName,
                courtesyCallDetail.TitanCompanyId,
                courtesyCallDetail.OfficeName, courtesyCallDetail.ContactFirstName, courtesyCallDetail.ContactLastName,
                courtesyCallDetail.ContactPhoneNumber
                );


            var createTicketRequest = new CSTS_External_CreateTicket_Input
            {
                ticketDescription = description,
                sourceSystemSubId = 3400,
                centreNumber = courtesyCallDetail.CentreNumber,
                productTypeID = courtesyCallDetail.ProductId,
                sourceSystemId = 7,
                companyName = courtesyCallDetail.TitanCompanyName,
                subCategoryId = 113,
                categoryId = 25,
                contactName = courtesyCallDetail.ContactFirstName + " " + courtesyCallDetail.ContactLastName,
                contactEmail = courtesyCallDetail.ContactEmail,
                titanCompanyId = courtesyCallDetail.TitanCompanyId,
                contactPhone = courtesyCallDetail.ContactPhoneNumber
            };

            string error;
            int ticketRef;

            courtesyCallCreate.CSTS_External_CreateTicket(createTicketRequest,
                DateTime.Now,
                out error,
                out ticketRef);

            var instanceId = GetInstance(ticketRef);
            const int timeout = 10000;
            var elapsed = 0;

            while ((instanceId == 0) && (elapsed < timeout))
            {
                Thread.Sleep(1000);
                elapsed += 1000;
                instanceId = GetInstance(ticketRef);
            }


            using (var connection = CreateConnection())
            {
                connection.Query<CourtesyCallDetail>("[processor].[Courtesy_CreateTicketFinalise]",
                    new {TicketReference = ticketRef}, commandType: CommandType.StoredProcedure);
            }
            Logger.Log("Ticket Created: " + ticketRef, 0);
            Logger.Log(courtesyCallDetail.CentreNumber + " " + courtesyCallDetail.TitanCompanyName, 0);
            Console.WriteLine(courtesyCallDetail.CentreNumber + " " + courtesyCallDetail.TitanCompanyName);
            Console.WriteLine(error + ", " + ticketRef);
        }

        private static int GetInstance(int ticketRef)
        {
            using (var connection = CreateConnection())
            {
                var instanceId = connection.Query<int>(
                    "select TWInstance from customerservice.ticket where ticketreference = @ticketRef",
                    new {ticketRef});
                return instanceId.FirstOrDefault();
            }
        }
    }
}